import User from "../models/userModel.js";
import bcrypt from "bcryptjs";
import Screen from "../models/screenModel.js";
import Media from "../models/mediaModel.js";
import { generateToken } from "../utils/authUtils.js";
import {
  sendConfirmationEmail,
  sendMailForThankYou,
} from "../utils/sendEmail.js";
import data from "../utils/data.js";
import Campaign from "../models/campaignModel.js";
import { ObjectId } from "mongodb";
import Wallet from "../models/walletModel.js";
import mongoose from "mongoose";
import { CAMPAIGN_STATUS_ACTIVE } from "../Constant/campaignStatusConstant.js";
import Coupon from "../models/couponModel.js";
import { returnUserSignIn } from "../helpers/userSignin.js";

export const pwaInstalledByUser = async (req, res) => {
  try {
    const user = await User.findOne({ _id: req.params.id });
    if (!user.pwaInstalled || user.pwaInstalled === false) {
      user.pwaInstalled = true;
      await user.save();
    }
    const pwaInstalledUser = await user.save();
    return res.status(200).send(pwaInstalledUser);
  } catch (error) {
    return res.status(404).send(error);
  }
};

const changePassword = async (req, res, user) => {
  try {
    user.password = bcrypt.hashSync(req.body.password, 8);
    const updatedUser = await user.save();
    return res.status(200).send(returnUserSignIn(updatedUser));
  } catch (error) {
    return res.status(404).send(error);
  }
};

export const updatePassword = async (req, res) => {
  try {
    const userId = req.params.id;
    const { oldPassword, newPassword } = req.body;
    const user = await User.findById(userId);
    if (bcrypt.compareSync(oldPassword, user.password)) {
      user.password = bcrypt.hashSync(newPassword, 8);
      const updatedUser = await user.save();
      return res.status(201).send("Password Change");
    } else {
      return res.status(404).send({ message: "Old Password incorrect!" });
    }
  } catch (error) {
    return res.status(404).send(error);
  }
};

export async function userSignUp(req, res) {
  try {
    const requestCameFromURL = `${req.header("Origin")}/`;
    const oldUser = await User.findOne({ email: req.body.email });
    if (oldUser && req.body.password) {
      changePassword(req, res, oldUser);
    }

    if (!oldUser && req.body.password) {
      const user = new User({
        name: req.body.name,
        email: req.body.email.toLowerCase(),
        password: bcrypt.hashSync(req.body.password, 8),
      });
      const createdUser = await user.save();
      console.log("user created  : ", createdUser);

      return res.status(200).send(returnUserSignIn(createdUser));
    }
  } catch (error) {
    return res.status(404).send(error);
  }
}
export async function filterUserListByName(req, res) {
  try {
    const searchString = String(req.params.name);
    console.log("name to filter : ", searchString);
    const users = await User.find({
      name: { $regex: searchString, $options: "i" },
    });
    console.log("users founds : ", users);

    return res.status(200).send(users);
  } catch (error) {
    return res
      .status(500)
      .send({ message: `User router error ${error.message}` });
  }
}

export async function getUserInfoById(req, res) {
  try {
    const user = await User.findOne(
      { _id: req.params.id },
      {
        name: 1,
        email: 1,
        phone: 1,
        address: 1,
        districtCity: 1,
        pincode: 1,
        stateUt: 1,
        country: 1,
      }
    );
    if (user) {
      // user.defaultWallet = req.params.walletAddress || user.defaultWallet;
      return res.status(200).send(user);
    } else {
      return res.status(404).send({ message: "User not found" });
    }
  } catch (error) {
    return res
      .status(500)
      .send({ message: `User router error ${error.message}` });
  }
}

// send email ti user to set passoword

export async function sendEmailToSetPassword(req, res) {
  try {
    console.log("request came to send email to user ", req.body.email);
    const requestCameFromURL = `${req.header("Origin")}/`;
    const email = req.body.email;
    const signup = req.body.signup;
    const forgetPassword = req.body.forgetPsssword;

    const user = await User.findOne({ email });

    if (user && signup) {
      return res
        .status(400)
        .send({ message: "User already exist, please sign in" });
    } else if (!user && forgetPassword) {
      return res
        .status(400)
        .send({ message: "Your account does not exist, please sign up first" });
    } else {
      sendConfirmationEmail(
        req.body.email,
        req.body.name,
        requestCameFromURL,
        req,
        res
      );
    }
  } catch (error) {
    return res.status(500).send({
      message: `sendEmailToSetPassword controller error ${error.message}`,
    });
  }
}
export async function userSigninWithGoogleLogin(req, res) {
  try {
    const requestCameFromURL = `${req.header("Origin")}/`;
    const email = req.body.email;
    let user = await User.findOne({ email });

    if (!user) {
      const newUser = new User({
        name: req.body.name,
        email: req.body.email,
        password: bcrypt.hashSync(`12345678`, 8),
        avatar: req.body.avatar,
      });
      user = await newUser.save();
      sendMailForThankYou({
        toEmail: email,
        userName: req.body.name,
        requestCameFromURL,
        password: `12345678`,
      });
    }

    return res.status(200).send(returnUserSignIn(user));
  } catch (error) {
    return res.status(500).send({
      message: `User router error userSigninWithGoogleLogin ${error.message}`,
    });
  }
}

export async function userSignin(req, res) {
  try {
    const email = req.body.email ? req.body.email.toLowerCase() : "";
    const user = await User.findOne({ email });
    if (!user) {
      return res
        .status(401)
        .send({ message: "New user, please sign up to continue" });
    }

    if (user && bcrypt.compareSync(req.body.password, user.password)) {
      return res.status(200).send(returnUserSignIn(user));
    } else {
      return res.status(402).send({ message: "Email or password incorrect" });
    }
  } catch (error) {
    console.error(error);
    return res.status(404).send(error);
  }
}

// get users list
export async function getUsersList(req, res) {
  try {
    const users = await User.find({});
    return res.status(200).send(users);
  } catch (error) {
    return res.status(404).send(error);
  }
}

export async function topCreators(req, res) {
  const topCreators = await User.find({
    isCreator: true,
  })
    .sort({ "creator.rating": -1 })
    .limit(5);
  res.status(200).send(topCreators);
}

export async function topMasters(req, res) {
  const topMasters = await User.find({
    isMaster: true,
  })
    .sort({ "master.rating": -1 })
    .limit(3);
  res.status(200).send(topMasters);
}
//getUserCampaigns
export async function getUserCampaigns(req, res) {
  try {
    const allyId = req.params.id;
    // first find all distinct  cid and campaign name
    const data = await Campaign.aggregate([
      { $match: { ally: new ObjectId(allyId) } },
      {
        $group: {
          _id: {
            cid: "$cid",
            campaignName: "$campaignName",
          },
        },
      },
    ]);

    if (data.length === 0) {
      return res.status(404).send({ message: "Campaign not found" });
    }

    const myCampaigns = [];

    for (let singleData of data) {
      const campaign = await Campaign.findOne({
        cid: singleData._id.cid,
        campaignName: singleData._id.campaignName,
      });
      if (campaign) {
        myCampaigns.push(campaign);
      }
    }

    const campaignsHere = myCampaigns
      .sort((objA, objB) => new Date(objA.startDate) - new Date(objB.startDate))
      .reverse();

    return res.status(200).send(campaignsHere);
  } catch (error) {
    return res.status(500).send({
      message: `User router error in getUserCampaigns ${error.message}`,
    });
  }
}

export async function getUserActiveCampaigns(req, res) {
  try {
    // console.log("getUserActiveCampaigns called!");
    const allyId = req.params.id;
    // first find all distinct  cid and campaign name
    const data = await Campaign.aggregate([
      { $match: { ally: new ObjectId(allyId), status: "Active" } },
      {
        $group: {
          _id: {
            cid: "$cid",
            campaignName: "$campaignName",
          },
        },
      },
    ]);

    if (data.length === 0) {
      return res.status(404).send({ message: "Campaign not found" });
    } else {
      const myCampaigns = [];

      for (let singleData of data) {
        const campaign = await Campaign.findOne({
          cid: singleData._id.cid,
          campaignName: singleData._id.campaignName,
          ally: allyId,
          status: CAMPAIGN_STATUS_ACTIVE,
        });
        if (campaign) {
          myCampaigns.push(campaign);
        }
      }
      const campaignsHere = myCampaigns
        .sort(
          (objA, objB) => new Date(objA.startDate) - new Date(objB.startDate)
        )
        .reverse();
      return res.status(200).send(campaignsHere);
    }
  } catch (error) {
    return res.status(500).send({
      message: `User router error in getUserCampaigns ${error.message}`,
    });
  }
}

//top barnds
export async function topBrand(req, res) {
  const topBrands = await User.find({
    isBrand: true,
  })
    .sort({ "brand.rating": -1 })
    .limit(7);
  res.status(200).send(topBrands);
}

// get screenList of a user
export async function getUserScreens(req, res) {
  try {
    const master = req.params.id;
    const myScreens = await Screen.find({ master: master });
    if (myScreens) return res.status(200).send(myScreens);
    return res.status(404).send({ message: "Screens not found" });
  } catch (error) {
    return res
      .status(500)
      .send({ message: `User router error ${error.message}` });
  }
}

// delete all of  medias for user deleteAllMedias
export async function deleteAllMedias(req, res) {
  try {
    const user = await User.findById(req.params.id);
    if (!user) {
      return res.status(401).send({
        message: "User not Found",
      });
    } else {
      const deletedMedia = await Media.deleteMany({ uploader: req.params.id });
      const updatedUser = await User.update(
        { uploader: req.params.id },
        { $set: { medias: [] } },
        { multi: true }
      );
      return res.status(200).send({
        message: "All media Deleted of user",
        medias: deletedMedia,
      });
    }
  } catch (error) {
    return res.status(404).send(error);
  }
}

export async function deleteUserWallet(req, res) {
  try {
    const user = await User.findById(req.params.id);
    const updatedUser = await user.update(
      { $set: { wallets: [] } },
      { multi: true }
    );
    const deletedWallet = Wallet.remove({ user: req.params.id });
  } catch (error) {
    return res
      .status(500)
      .send({ message: `Campaign router error ${error.message}` });
  }
}

// get user medias
export async function getUserMedias(req, res) {
  try {
    const mymedias = await Media.find({ uploader: req.params.id });
    if (mymedias.length > 0) return res.status(200).send(mymedias);
    else return res.status(401).send({ message: "medias not found" });
  } catch (error) {
    return res
      .status(500)
      .send({ message: `User router error ${error.message}` });
  }
}

// seed data
export async function seedData(req, res) {
  const createdUsers = await User.insertMany(data.users);
  res.status(200).send({ createdUsers });
}

// user default wallet
export async function getDefaultWallet(req, res) {
  const user = await User.findById(req.params.id);
  if (user) {
    user.defaultWallet = req.params.walletAddress || user.defaultWallet;
    return res.status(200).send({ user });
  } else {
    return res.status(404).send({ message: "User not found" });
  }
}

// update user profile by himself/herself
export async function updateUserProfile(req, res) {
  try {
    const user = await User.findById(req.user._id);
    if (user) {
      if (req.body.password) {
        user.password = bcrypt.hashSync(req.body.password, 8);
      }
      user.name = req.body.name || user.name;
      user.email = req.body.email || user.email;
      user.phone = req.body.phone || user.phone;
      user.avatar = req.body.avatar || user.avatar;
      user.address = req.body.address || user.address;
      user.districtCity = req.body.districtCity || user.districtCity;
      user.municipality = req.body.municipality || user.municipality;
      user.pincode = req.body.pincode || user.pincode;
      user.stateUt = req.body.stateUt || user.stateUt;
      user.country = req.body.country || user.country;
      user.isMaster = req.body.isMaster || user.isMaster;
      user.isCreator = req.body.isCreator || user.isCreator;
      user.isBrand = req.body.isBrand;
      const updatedUser = await user.save();
      return res.status(200).send(returnUserSignIn(updatedUser));
    }
  } catch (error) {
    return res.status(404).send(error);
  }
}

export async function filterUserByNameOrEmailOrPhone(req, res) {
  try {
    const searchString = req.params.searchString.trim();
    console.log("filterUserByNameOrEmailOrPhone called :", searchString);

    const nameFilter = { name: { $regex: searchString, $options: "i" } };
    const emailFilter = {
      email: { $regex: searchString, $options: "i" },
    };

    const users = await User.find({
      $or: [nameFilter, emailFilter],
      isMaster: true,
    });
    // console.log("users : ", users.length);

    return res.status(200).send(users);
  } catch (error) {
    return res.status(404).send(`Errro in filterUser ${error}`);
  }
}

export async function getUserWishlist(req, res) {
  try {
    const user = await User.findById(req.params.userId);
    if (!user) return res.status(404).send({ message: "User not found!" });
    const coupons = await Coupon.find({ _id: { $in: user.wishlist } });
    return res.status(200).send(coupons);
  } catch (error) {
    return res.status(500).send({
      message: `Error in getUserWishlist ${error.message}`,
    });
  }
}

//  user delete
export async function deleteUser(req, res) {
  try {
    const user = await User.findById(req.params.id);
    if (!user) {
      return res.status(401).send({
        message: "User not Found",
      });
    }
    if (user.email === "vviicckkyy55@gmail.com") {
      return res.status(400).send({
        message: "Cannot delete admin father",
      });
    }
    const deleteUser = await user.remove();
    return res.status(200).send({
      message: "User Deleted",
      user: deleteUser,
    });
  } catch (error) {
    return res.status(404).send(error);
  }
}

// update user by isItanimulli user
export async function updateUser(req, res) {
  try {
    const user = await User.findById(req.params.id);
    if (!user) {
      return res.status(401).status(404).send({
        message: "User Not found",
      });
    }
    user.name = req.body.name || user.name;
    user.email = req.body.email || user.email;
    user.isMaster = Boolean(req.body.isMaster);
    user.isItanimulli = Boolean(req.body.isItanimulli);
    user.isCreator = Boolean(req.body.isCreator);
    user.isBrand = Boolean(req.body.isBrand);
    user.isViewer = Boolean(req.body.isViewer);
    user.pleasMade = req.body.pleasMade || user.pleasMade;
    user.alliedScreens = req.body.alliedScreens || user.alliedScreens;
    const updatedUser = await user.save();
    return res.status(200).send({
      message: "User Updated",
      user: updatedUser,
    });
  } catch (error) {
    return res.status(404).send(error);
  }
}
