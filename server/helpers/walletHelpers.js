import User from "../models/userModel.js";
import Campaign from "../models/campaignModel.js";
import UserWallet from "../models/userWalletModel.js";
import mongoose from "mongoose";
import { addNewWallet } from "../controllers/walletController.js";

export const deductAmountFromWalletForCreateCampaign = async (campaign) => {
  try {
    const ally = campaign.ally;

    const walletUser = await UserWallet.findOne({ user: ally });
    console.log("wallet user : ", walletUser);
    if (!walletUser) {
      Promise.reject("User has no wallet");
    }
    // reduce balance to campaign.totalAmount
    walletUser.balance = Number(walletUser.balance - campaign.totalAmount);

    const newTransaction = {
      _id: new mongoose.Types.ObjectId(),
      txId: new mongoose.Types.ObjectId(),
      txDate: new Date(),
      txType: "DABIT",
      success: true,
      amount: campaign.totalAmount,
      aditionalInfo: {
        campaign: campaign._id,
      },
    };

    walletUser.transactions.push(newTransaction);

    const updatedWallet = await walletUser.save();
    console.log("updatedWallet : ", updatedWallet);
    return Promise.resolve();
  } catch (error) {
    console.error("Error : ", error);
    return Promise.reject(error);
  }
};
